cmake_minimum_required(VERSION 2.8)
project(ORB_SLAM2)

IF(NOT CMAKE_BUILD_TYPE)
  SET(CMAKE_BUILD_TYPE Release)
ENDIF()

MESSAGE("Build type: " ${CMAKE_BUILD_TYPE})

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS}  -Wall  -O3 -march=native ")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall   -O3 -march=native")

# Check C++11 or C++0x support
include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
CHECK_CXX_COMPILER_FLAG("-std=c++0x" COMPILER_SUPPORTS_CXX0X)
if(COMPILER_SUPPORTS_CXX11)
   set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
   add_definitions(-DCOMPILEDWITHC11)
   message(STATUS "Using flag -std=c++11.")
elseif(COMPILER_SUPPORTS_CXX0X)
   set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
   add_definitions(-DCOMPILEDWITHC0X)
   message(STATUS "Using flag -std=c++0x.")
else()
   message(FATAL_ERROR "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
endif()

LIST(APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake_modules)
set(OpenCV_DIR /opt/ros/kinetic/share/OpenCV-3.3.1-dev)
#set(OpenCV_DIR /home/zhaiyujia/opencv-3.4.7/build)
find_package(OpenCV REQUIRED)
include_directories( ${OpenCV_INCLUDE_DIRS} )


find_package(Eigen3  REQUIRED)
find_package(Pangolin REQUIRED)

find_package(Boost REQUIRED COMPONENTS thread)
if(Boost_FOUND)
   message("Boost was found!")
   message("Boost Headers DIRECTORY: " ${Boost_INCLUDE_DIRS})
   message("Boost LIBS DIRECTORY: " ${Boost_LIBRARY_DIRS})
   message("Found Libraries: " ${Boost_LIBRARIES})
endif()


include_directories(/usr/local/lib/python3.6
        /usr/local/include/python3.6m)
link_directories(/usr/local/lib/python3.6/config-3.6m-x86_64-linux-gnu)


#set(PythonLibs_DIR /usr/local/lib/python3.6)
#find_package(PythonLibs REQUIRED)
#if (NOT PythonLibs_FOUND)
#   message(FATAL_ERROR "PYTHON LIBS not found.")
#else()
#   message("PYTHON LIBS were found!")
#   message("PYTHON LIBS DIRECTORY: " ${PYTHON_LIBRARY})
#endif()

include_directories(
        ${PROJECT_SOURCE_DIR}
        ${PROJECT_SOURCE_DIR}/include
        ${EIGEN3_INCLUDE_DIR}
        ${Pangolin_INCLUDE_DIRS}
        ${Boost_INCLUDE_DIRS}
#        /home/zhaiyujia/.local/lib/python3.6/site-packages/numpy/core/include/numpy/
)

set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/lib)

add_library(${PROJECT_NAME} SHARED
         src/System.cc
         src/Tracking.cc
         src/LocalMapping.cc
         src/LoopClosing.cc
         src/ORBextractor.cc
         src/ORBmatcher.cc
         src/FrameDrawer.cc
         src/Converter.cc
         src/MapPoint.cc
         src/KeyFrame.cc
         src/Map.cc
         src/MapDrawer.cc
         src/Optimizer.cc
         src/PnPsolver.cc
         src/Frame.cc
         src/KeyFrameDatabase.cc
         src/Sim3Solver.cc
         src/Initializer.cc
         src/Viewer.cc
        src/DetectCNN.cpp
)

target_link_libraries(${PROJECT_NAME}
        ${OpenCV_LIBS}
        ${EIGEN3_LIBS}
        ${Pangolin_LIBRARIES}
        ${PROJECT_SOURCE_DIR}/Thirdparty/DBoW2/lib/libDBoW2.so
        ${PROJECT_SOURCE_DIR}/Thirdparty/g2o/lib/libg2o.so
        /usr/lib/python3.6/config-3.6m-x86_64-linux-gnu/libpython3.6.so
        ${Boost_LIBRARIES}
)

#set(PYTHON_INCLUDE_DIRS ${PYTHON_INCLUDE_DIRS} /home/zhaiyujia/.local/lib/python3.6/site-packages/numpy/core/include/numpy)
set(PYTHON_INCLUDE_DIRS ${PYTHON_INCLUDE_DIRS} /home/zhaiyujia/4_CLionProjects/ORB_SLAM2_CNN/Yet-Another-EfficientDet-Pytorch)

# Build examples

#set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/Examples/RGB-D)
#
#add_executable(rgbd_tum
#Examples/RGB-D/rgbd_tum.cc)
#target_link_libraries(rgbd_tum ${PROJECT_NAME})
#
#set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/Examples/Stereo)
#
#add_executable(stereo_kitti
#Examples/Ster/home/zhaiyujia/1_SLAM_Project/ORB_SLAM2/CMakeLists.txteo/stereo_kitti.cc)
#target_link_libraries(stereo_kitti ${PROJECT_NAME})
#
#add_executable(stereo_euroc
#Examples/Stereo/stereo_euroc.cc)
#target_link_libraries(stereo_euroc ${PROJECT_NAME})


set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/Examples/Monocular)

add_executable(mono_tum
Examples/Monocular/mono_tum.cc)
target_link_libraries(mono_tum ${PROJECT_NAME})

#add_executable(mono_kitti
#Examples/Monocular/mono_kitti.cc)
#target_link_libraries(mono_kitti ${PROJECT_NAME})
#
add_executable(mono_euroc
Examples/Monocular/mono_euroc.cc)
target_link_libraries(mono_euroc ${PROJECT_NAME})
#
#add_executable(p9_office
#Examples/Monocular/p9_office.cpp)
#target_link_libraries(p9_office ${PROJECT_NAME})

add_executable(mono_test
        Examples/Monocular/mono_test.cpp)
target_link_libraries(mono_test ${PROJECT_NAME})