/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/


#include<iostream>
#include<algorithm>
#include<fstream>
#include<chrono>

#include<opencv2/core/core.hpp>

#include<System.h>
#include <unistd.h>
using namespace std;

void LoadImages(const string &strFile, vector<string> &vstrImageFilenames,
                vector<double> &vTimestamps);

int main(int argc, char **argv)
{
//    if(argc != 4)
//    {
//        cerr << endl << "Usage: ./mono_tum path_to_vocabulary path_to_settings path_to_sequence" << endl;
//        return 1;
//    }

    argv[1]="/home/zhaiyujia/1_SLAM_Project/ORB_SLAM2/Vocabulary/ORBvoc.txt";
    argv[2]="/home/zhaiyujia/1_SLAM_Project/ORB_SLAM2/Examples/Monocular/TUM2.yaml";
//    argv[2]="/home/zhaiyujia/1_SLAM_Project/ORB_SLAM2/Examples/Monocular/EuRoC.yaml";
//    argv[3]="/home/zhaiyujia/Pictures/TUM/Testing and Debugging/rgbd_dataset_freiburg1_xyz";
//    argv[3]="/home/zhaiyujia/Pictures/TUM/Handheld SLAM/rgbd_dataset_freiburg3_long_office_household";
//    argv[3]="/home/zhaiyujia/Pictures/TUM/Dynamic Objects/rgbd_dataset_freiburg2_desk_with_person";
//    argv[3]="/home/zhaiyujia/Pictures/TUM/Robot SLAM/rgbd_dataset_freiburg2_pioneer_360";
//  argv[3] = "/home/zhaiyujia/Pictures/TUM/Testing and Debugging/rgb
//  wostvd_dataset_freiburg1_xyz";
//  argv[3] = "/home/zhaiyujia/Pictures/TUM/Handheld_SLAM/rgbd_dataset_freiburg3_long_office_household";
//  argv[3] = "/home/zhaiyujia/Pictures/TUM/Handheld SLAM/rgbd_dataset_freiburg1_desk";
//  argv[3] = "/home/zhaiyujia/Pictures/TUM/Handheld SLAM/rgbd_dataset_freiburg1_floor";
//  argv[3] = "/home/zhaiyujia/Pictures/TUM/Handheld SLAM/rgbd_dataset_freiburg2_360_kidnap";
//  argv[3] = "/home/zhaiyujia/Pictures/TUM/Structure vs. Texture/rgbd_dataset_freiburg3_nostructure_texture_near_withloop";
//  argv[3] = "/home/zhaiyujia/Pictures/TUM/Structure vs. Texture/rgbd_dataset_freiburg3_structure_texture_far";
//  argv[3] = "/home/zhaiyujia/Pictures/TUM/Dynamic Objects/rgbd_dataset_freiburg3_sitting_xyz";
//    argv[3] = "/home/zhaiyujia/Pictures/TUM/Dynamic_Objects/rgbd_dataset_freiburg2_desk_with_person";
//  argv[3] = "/home/zhaiyujia/Pictures/TUM/Dynamic Objects/rgbd_dataset_freiburg3_sitting_halfsphere";
//  argv[3] = "/home/zhaiyujia/Pictures/TUM/Dynamic Objects/rgbd_dataset_freiburg3_walking_halfsphere";
//  argv[3] = "/home/zhaiyujia/Pictures/TUM/Testing and Debugging/rgbd_dataset_freiburg2_rpy";
//    argv[3] = "/home/zhaiyujia/Pictures/TUM/Handheld SLAM/rgbd_dataset_freiburg2_desk";
//    argv[3] = "/home/zhaiyujia/Pictures/TUM/Structure vs. Texture/rgbd_dataset_freiburg3_nostructure_texture_far";
//    argv[3] = "/home/zhaiyujia/Pictures/TUM/Structure vs. Texture/rgbd_dataset_freiburg3_nostructure_texture_near_withloop";
//    argv[3] = "/home/zhaiyujia/Pictures/EuRoC/MH_01_easy/mav0";
    argv[3]="/home/zhaiyujia/Pictures/CMU/mono/ME001/";

//    // Retrieve paths to images
    vector<string> vstrImageFilenames;
    vector<double> vTimestamps;
//    string strFile = string(argv[3])+"/rgb.txt";
//    LoadImages(strFile, vstrImageFilenames, vTimestamps);
//
//    int nImages = vstrImageFilenames.size();
    int nImages = 100000;
//    // Create SLAM system. It initializes all system threads and gets ready to process frames.
    ORB_SLAM2::System SLAM(argv[1],argv[2],ORB_SLAM2::System::MONOCULAR,true);
//
//    // Vector for tracking time statistics
    vector<float> vTimesTrack;
    vTimesTrack.resize(nImages);
//
    cout << endl << "-------" << endl;
//    cout << "Start processing sequence ..." << endl;
//    cout << "Images in the sequence: " << nImages << endl << endl;
//
//    ofstream timetrack;
//    timetrack.open("/home/zhaiyujia/Code/OFS-SLAM/result/ORBSLAM2/timetrack.txt",ios::trunc);

    for(int i=0; i<100000; i++)
    {
        stringstream ss;
        ss << setfill('0') << setw(6) << i;
        string imgnames =ss.str() + ".png";
        vTimestamps.push_back(i);
        vstrImageFilenames.push_back(imgnames);
    }

    cerr<<"dcewac";

    // Main loop
    cv::Mat im;
//    for(int ni=0; ni<nImages; ni++)
    for(int ni=0; ni<100000; ni++)
    {
        // Read image from file
        im = cv::imread(string(argv[3])+"/"+vstrImageFilenames[ni],CV_LOAD_IMAGE_UNCHANGED);
        double tframe = vTimestamps[ni];

        if(im.empty())
        {
            cerr << endl << "Failed to load image at: "
                 << string(argv[3]) << "/" << vstrImageFilenames[ni] << endl;
            return 1;
        }

#ifdef COMPILEDWITHC11
        std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
#else
        std::chrono::monotonic_clock::time_point t1 = std::chrono::monotonic_clock::now();
#endif

        // Pass the image to the SLAM system
        SLAM.TrackMonocular(im,tframe);

#ifdef COMPILEDWITHC11
        std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
#else
        std::chrono::monotonic_clock::time_point t2 = std::chrono::monotonic_clock::now();
#endif

        double ttrack= std::chrono::duration_cast<std::chrono::duration<double> >(t2 - t1).count();

        vTimesTrack[ni]=ttrack;

//        timetrack<<fixed<<tframe<<"     "<<vTimesTrack[ni]*1000000<<endl;

        // Wait to load the next frame
        double T=0;
        if(ni<nImages-1)
            T = vTimestamps[ni+1]-tframe;
        else if(ni>0)
            T = tframe-vTimestamps[ni-1];

        if(ttrack<T)
            usleep((T-ttrack)*1e6);

    }

    // Stop all threads
    SLAM.Shutdown();

    // Tracking time statistics
    sort(vTimesTrack.begin(),vTimesTrack.end());
    float totaltime = 0;
    for(int ni=0; ni<nImages; ni++)
    {
        totaltime+=vTimesTrack[ni];
    }
    cout << "-------" << endl << endl;
    cout << "median tracking time: " << vTimesTrack[nImages/2] << endl;
    cout << "mean tracking time: " << totaltime/nImages << endl;

    // Save camera trajectory
    SLAM.SaveTrajectoryTUM("/home/zhaiyujia/Code/OFS-SLAM/result/ORBSLAM2/FrameTrajectory.txt");
    SLAM.SaveKeyFrameTrajectoryTUM("/home/zhaiyujia/Code/OFS-SLAM/result/ORBSLAM2/keyFrameTrajectory");

    return 0;
}

void LoadImages(const string &strFile, vector<string> &vstrImageFilenames, vector<double> &vTimestamps)
{
    ifstream f;
    f.open(strFile.c_str());

    // skip first three lines
    string s0;
    getline(f,s0);
    getline(f,s0);
    getline(f,s0);

    while(!f.eof())
    {
        string s;
        getline(f,s);
        if(!s.empty())
        {
            stringstream ss;
            ss << s;
            double t;
            string sRGB;
            ss >> t;
            vTimestamps.push_back(t);
            ss >> sRGB;
            vstrImageFilenames.push_back(sRGB);
        }
    }
}
