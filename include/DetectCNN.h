//
// Created by zhaiyujia on 2020/11/6.
//

#ifndef ORB_SLAM2_DETECTCNN_H
#define ORB_SLAM2_DETECTCNN_H

#include <python3.6m/Python.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <vector>
#include <cstdio>
#include <boost/thread.hpp>
#include "Conversion.h"
//#include "ndarrayobject.h"

namespace DetectCNN
{

    class DetectDynObject{
    private:
        DynaSLAM::NDArrayConverter *cvt; 	/*!< Converter to NumPy Array from cv::Mat */
        PyObject *py_module; 	/*!< Module of python where the Mask algorithm is implemented */
        PyObject *py_function; 	/*!< Class to be instanced */
        PyObject *net; 			/*!< Instance of the class */
        std::string py_path; 	/*!< Path to be included to the environment variable PYTHONPATH */
        std::string module_name; /*!< Detailed description after the member */
        std::string function_name; /*!< Detailed description after the member */
        std::string img_path; 	/*!< Detailed description after the member */

        void ImportSettings();
    public:

        DetectDynObject();
        ~DetectDynObject();
       void GetSegmentation(std::string rgb_name);
    };


}

#endif //ORB_SLAM2_DETECTCNN_H
