/**
* This file is part of DynaSLAM.
*
* Copyright (C) 2018 Berta Bescos <bbescos at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/bertabescos/DynaSLAM>.
*
*/

#include "DetectCNN.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <dirent.h>
#include <errno.h>

namespace DetectCNN
{

#define U_SEGSt(a)\
    gettimeofday(&tvsv,0);\
    a = tvsv.tv_sec + tvsv.tv_usec/1000000.0
    struct timeval tvsv;
    double t1sv, t2sv,t0sv,t3sv;
    void tic_initsv(){U_SEGSt(t0sv);}
    void toc_finalsv(double &time){U_SEGSt(t3sv); time =  (t3sv- t0sv)/1;}
    void ticsv(){U_SEGSt(t1sv);}
    void tocsv(){U_SEGSt(t2sv);}
// std::cout << (t2sv - t1sv)/1 << std::endl;}

    DetectDynObject::DetectDynObject(){
//        std::cout << "Importing Mask R-CNN Settings..." << std::endl;
//        ImportSettings();
        this->py_path = "/home/zhaiyujia/4_CLionProjects/ORB_SLAM2_CNN/Yet-Another-EfficientDet-Pytorch";
        this->module_name = "efficientdet_test";
        this->function_name = "segment_efficientdet";
        this->img_path;
        std::string x;
        setenv("PYTHONPATH", this->py_path.c_str(), 1);
        x = getenv("PYTHONPATH");
        Py_Initialize();
//        this->cvt = new DynaSLAM::NDArrayConverter();
        this->py_module = PyImport_ImportModule(this->module_name.c_str()); //调用名为 this->module_name.c_str() 的 py 程序。
        if(PyErr_Occurred()) PyErr_Print();
        assert(this->py_module != NULL);
        this->py_function = PyObject_GetAttrString(this->py_module, this->function_name.c_str());//得到this->py_module 程序中的 函数this->function_name.c_str()的PyObjet指针 。
//        assert(this->py_function != NULL);
//        this->net = PyInstance_New(this->py_function, NULL, NULL); //构造一个this->py_class的python对象
//        if(PyErr_Occurred()) PyErr_Print();
//        assert(this->net != NULL);
//        std::cout << "Creating net instance..." << std::endl;
//        cv::Mat image  = cv::Mat::zeros(480,640,CV_8UC3); //Be careful with size!!
//        std::cout << "Loading net parameters..." << std::endl;
        if(PyErr_Occurred()) PyErr_Print();
//        GetSegmentation();
        if(PyErr_Occurred()) PyErr_Print();
        std::cout << "DetectDynObject\n";
    }

    DetectDynObject::~DetectDynObject(){
        delete this->py_module;
        delete this->py_function;
        delete this->net;
//        delete this->cvt;
    }

    void DetectDynObject::GetSegmentation(std::string name){
//        cv::Mat seg = cv::imread(dir+"/"+name,CV_LOAD_IMAGE_UNCHANGED);
//        if(seg.empty()){
//            PyObject* py_image = cvt->toNDArray(image.clone()); //将cvmat转为一个numpy array
//            assert(py_image != NULL);
            //调用了刚才 this->net 类中的const_cast<char*>(this->img_path.c_str())函数，并给函数传参
//            PyObject_CallMethod(pClass, “class_method”, “O”, pInstance)
            //参数分别为 PyObject（类），string（类方法），string（O表示参数为PyObject） ，PyObject（类实例）
            //返回一个py实例py_mask_image是一个numpy数组,即python函数return的mask(检测到的目标像素值为1，其余为0 ）
            PyObject* py_boxs = PyObject_CallFunction(this->py_function, "s", name.c_str());

            int size = PyList_Size(py_boxs);
            std::cout<<"List size: "<<size<<std::endl;
            assert(size == 1);

            PyObject *py_dict = PyList_GetItem(py_boxs, 0);
            assert(PyDict_Check(py_dict));
            size = PyDict_Size(py_dict);
            assert(size == 3);
            PyObject *py_rois = PyDict_GetItemString(py_dict,"rois");
            PyObject *py_labels = PyDict_GetItemString(py_dict,"class_ids");
            PyObject *py_scores = PyDict_GetItemString(py_dict,"scores");

            assert(PyList_Check(py_rois));
            float element;
            std::vector<std::pair<int,std::vector<float>>> objects;
            std::vector<float> object_scores;
            PyObject* py_element;
            for(int i = 0; i< PyList_Size(py_rois); i++)
            {
                std::vector<float> object_boxes;
                std::cout<<"object nums="<<PyList_Size(py_rois);
                PyObject* py_box  = PyList_GetItem(py_rois, i);
                assert(PyList_Check(py_box));
                int size = PyList_Size(py_box);
                assert(size == 4);
                for (int j = 0; j < 4; ++j)
                {
                    py_element = PyList_GetItem(py_box, j);
                    PyArg_Parse(py_element, "f", &element);
                    object_boxes.push_back(element);
                    std::cout<<"object_box"<<element;
                }
                assert(PyList_Check(py_labels));
                PyObject* plabel =  PyList_GetItem(py_labels,i);
                int label;
                PyArg_Parse(plabel,"i",&label);
                objects.push_back({label,object_boxes});

                PyObject* pscore =  PyList_GetItem(py_scores,i);
                float score;
                PyArg_Parse(pscore,"f",&score);
                object_scores.push_back(score);
            }
            std::cout<<"read object_boxes done!"<<std::endl;


//            assert(PyArray_Check(py_rois) );


//            int typenum = PyArray_TYPE(py_rois);
//            int type = typenum == NPY_UBYTE ? CV_8U :
//                       typenum == NPY_BYTE ? CV_8S :
//                       typenum == NPY_USHORT ? CV_16U :
//                       typenum == NPY_SHORT ? CV_16S :
//                       typenum == NPY_INT || typenum == NPY_LONG ? CV_32S :
//                       typenum == NPY_FLOAT ? CV_32F :
//                       typenum == NPY_DOUBLE ? CV_64F : -1;
//
//            int ndims = PyArray_NDIM(py_rois);
//            std::cout<<"type"<<type<<std::endl;
//            std::cout<<"nuims"<<ndims<<std::endl;



    //        seg = cvt->toMat(py_boxes).clone(); //将mask转换成cvmat
//            seg.cv::Mat::convertTo(seg,CV_8U);//0 background y 1 foreground
//            if(dir.compare("no_save")!=0){
//                DIR* _dir = opendir(dir.c_str());
//                if (_dir) {closedir(_dir);}
//                else if (ENOENT == errno)
//                {
//                    const int check = mkdir(dir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
//                    if (check == -1) {
//                        std::string str = dir;
//                        str.replace(str.end() - 6, str.end(), "");
//                        mkdir(str.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
//                    }
//                }
//                cv::imwrite(dir+"/"+img,seg);
//            }
//        }
//        return seg;
    }

    void DetectDynObject::ImportSettings(){
//        std::string strSettingsFile = "../RGB-D/MaskSettings.yaml";
//        cv::FileStorage fs(strSettingsFile.c_str(), cv::FileStorage::READ);
//        fs["py_path"] >> this->py_path;
//        fs["module_name"] >> this->module_name;
//        fs["function_name"] >> this->function_name;
//        fs["img_path"] >> this->img_path;

//
//        std::cout << "    py_path: "<< this->py_path << std::endl;
//        std::cout << "    module_name: "<< this->module_name << std::endl;
//        std::cout << "    function_name: " << this->function_name << std::endl;
//        std::cout << "    img_path: " << this->img_path << std::endl;
    }


}






















